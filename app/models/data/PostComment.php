<?php

namespace Shakil\Models\Data;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as Capsule;

class PostComment extends Model {

	public function getAuthorName(){
		return Capsule::table('users')->where('id', $this->user_id)->first()->username ?? 'N/A';

	}

    public function post()
    {
        return $this->belongsTo('Shakil\Models\Data\Post');
    }
}
