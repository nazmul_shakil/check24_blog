
<div style="width: 80%;margin: 0 auto;">
    <div>
        <a href="<?php echo URLROOT; ?>/index"><img src="https://via.placeholder.com/150" alt="logo" style="float: left;width: 98px;"></a>
        <p style="position: relative;top: 35px;">Check24 Blog</p>
    </div>
</div>
<div class="nav-holder">
    <div >
        <ul>
            <li>
                <a href="<?php echo URLROOT; ?>/index">Home</a>
            </li>
            <li>
                <?php if(isset($_SESSION['user_id'])) : ?>
                    <a href="<?php echo URLROOT; ?>/posts/create">Create Post</a>
                <?php endif; ?>
            </li>
            <li>
                <a href="<?php echo URLROOT; ?>/posts">Posts</a>
            </li>
        </ul>
        <div class="login-area">
            <?php if(isset($_SESSION['user_id'])) : ?>
                <a href="<?php echo URLROOT; ?>/users/logout">Log out</a>
            <?php else : ?>
                <a href="<?php echo URLROOT; ?>/users/login">Login</a>
            <?php endif; ?>
        </div>
        
    </div>
    
</div>

