<?php
   require APPROOT . '/views/includes/head.php';
?>

<div class="navbar">
    <?php
       require APPROOT . '/views/includes/navigation.php';
    ?>
</div>

<div class="container">
    <?php $post = $data['post']; ?>
    <div class="container-item" >
        <div >
            <h3>
                <?php echo 'Created on: ' . date('F j h:m', strtotime($post->created_at)); ?>
            </h3>
            <h3>
                <?php echo $post->title; ?>
            </h3>
            <img src="<?php echo $post->image_link; ?>" alt="post image" style="width: 100%;">
        </div>

        <div>
            <p>
                <?php echo html_entity_decode(htmlentities($post->body)); ?>
            </p>
            <span>Author : <?php echo $post->getAuthorName();?></span>
        </div>

        <?php if(isLoggedIn()): ?>
        <div style="background: #e9e9e9;padding: 5px;margin: 5px;">
            <h3>Comments</h3>
            <?php 
            if(sizeof($post->comments) > 0){
                foreach($post->comments as $comment){
           
            ?>
            
            <p><?php echo $comment->name . '(' . date('F j h:m', strtotime($comment->created_at)) .') : ' . $comment->comment;?></p>
            <?php }  } ?>
            
        </div>
        <?php endif; ?>
    </div>

</div>
