<?php
   require APPROOT . '/views/includes/head.php';
?>

<div class="navbar">
    <?php
       require APPROOT . '/views/includes/navigation.php';
    ?>
</div>

<div class="container center">
    <h1>
        Create new post
    </h1>

    <form action="<?php echo URLROOT; ?>/posts/create" method="POST">
        <div class="form-item">
            <input type="text" name="title" placeholder="Title..." required>

            <span class="invalidFeedback">
                <?php echo $data['titleError']; ?>
            </span>
        </div>
        <div class="form-item">
            <input type="text" name="image_link" placeholder="Image Link..." required>
        </div>

        <div class="form-item">
            <div >
                <textarea id="post_body" name="body" placeholder="Enter your post..." rows="5" cols="5"></textarea>
            </div>

            <span class="invalidFeedback">
                <?php echo $data['bodyError']; ?>
            </span>
        </div>

        <button class="btn green" name="submit" type="submit">Submit</button>
    </form>
</div>

<script>

      tinymce.init({
        selector: '#post_body'
      });

</script>




