<?php
   require APPROOT . '/views/includes/head.php';
?>

<div class="navbar">
    <?php
       require APPROOT . '/views/includes/navigation.php';
    ?>
</div>

<div class="container">
    <?php foreach($data['posts'] as $post): ?>
        <div class="container-item" >
            <a href="<?php echo URLROOT . "/posts/details/" . $post->id ?>" style="width: 100%;display: flex;">
            <div style="width: 80%;display: inline-block;" class="single-item">
                <h3>
                    <?php echo 'Created on: ' . date('F j h:m', strtotime($post->created_at)) .' - '. $post->title; ?>
                </h3>

                <p class="post_body">
                    <?php 
                    $getPostBody = $post->body;
                    $max_count = 1000;
                    if(strlen($getPostBody) > $max_count){
                        $getPostBody = substr($post->body, 0, $max_count) . ' ...';
                    }

                    ?>
                    <?php echo html_entity_decode(htmlentities($getPostBody)); ?>
                </p>
                <span>Author : <?php echo $post->getAuthorName();?></span>
                <br>
                <span>
                    Post comment counts : <?php echo $post->comments()->count();?>
                </span>
            </div>
            <div style="width: 20%;">
                <img src="<?php echo $post->image_link; ?>" alt="post image" style="width: 250px;">
            </div>
            </a>
            
        </div>
    <?php endforeach; ?>

    <div>

        <?php 
         $currentPage = $data['posts']->currentPage(); 
         $lastPage = $data['posts']->lastPage(); 
        ?>

         
        <?php if($currentPage != 1){?>
        <a href="<?php echo URLROOT; ?>/posts?page=<?php echo $currentPage - 1; ?>">Prev</a>
        <?php }?>

        Page <?php echo $currentPage; ?>

        <?php if($currentPage < $lastPage){?>
        <a href="<?php echo URLROOT; ?>/posts?page=<?php echo $currentPage + 1; ?>">Next</a>
        <?php }?>
    </div>
</div>
