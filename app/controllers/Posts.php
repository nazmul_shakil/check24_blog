<?php
use Shakil\Models\Data\Post;
class Posts extends Controller {
    public function __construct() {

    }

    public function index() {
        $posts = Post::where('is_deleted',0)->paginate(3);

        $data = [
            'posts' => $posts
        ];

        $this->view('posts/index', $data);
    }

    public function create() {
        if(!isLoggedIn()) {
            header("Location: " . URLROOT . "/posts");
        }

        $data = [
            'title' => '',
            'body' => '',
            'titleError' => '',
            'bodyError' => ''
        ];

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'user_id' => $_SESSION['user_id'],
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'image_link' => trim($_POST['image_link']),
                'titleError' => '',
                'bodyError' => ''
            ];

            if(empty($data['title'])) {
                $data['titleError'] = 'The title of a post cannot be empty';
            }

            if(empty($data['body'])) {
                $data['bodyError'] = 'The body of a post cannot be empty';
            }

            if (empty($data['titleError']) && empty($data['bodyError'])) {
                $post =  new Post;
                $post->user_id = $_SESSION['user_id'];
                $post->title = $_POST['title'];     
                $post->body = $_POST['body'];     
                $post->image_link = $_POST['image_link'];  
                if ($post->save()) {
                    header("Location: " . URLROOT . "/posts");
                } else {
                    die("Something went wrong, please try again!");
                }
            } else {
                $this->view('posts/create', $data);
            }
        }

        $this->view('posts/create', $data);
    }

    public function details($id){
        $post = Post::with('comments')->find($id);

        $data = [
            'post' => $post
        ];

        $this->view('posts/details', $data);
    }
}

