<?php
  /*
   * App Core Class
   * Creates URL & loads core controller
   * URL FORMAT - /controller/method/params
   */

  use Illuminate\Database\Capsule\Manager as Capsule;

  class Core {
    protected $currentController = 'Pages';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct(){
      //print_r($this->getUrl());

      $capsule = new Capsule();

      // TODO require and use config file
      $capsule->addConnection([
          'driver'    => 'mysql',
          'host'      => DB_HOST,
          'database'  => DB_NAME,
          'username'  => DB_USER,
          'password'  => DB_PASS,
          'charset'   => 'utf8',
          'collation' => 'utf8_unicode_ci',
          'prefix'    => '',
      ]);

      // Make this Capsule instance available globally via static methods... (optional)
      $capsule->setAsGlobal();

      // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
      $capsule->bootEloquent();

      \Illuminate\Pagination\Paginator::currentPageResolver(function ($pageName = 'page') {
          return (int) ($_GET[$pageName] ?? 1);
      });

      $url = $this->getUrl();

      // Look in BLL for first value
      if(file_exists('../app/controllers/' . ucwords($url[0]). '.php')){
        // If exists, set as controller
        $this->currentController = ucwords($url[0]);
        // Unset 0 Index
        unset($url[0]);
      }

      // Require the controller
      require_once '../app/controllers/'. $this->currentController . '.php';

      // Instantiate controller class
      $this->currentController = new $this->currentController;

      // Check for second part of url
      if(isset($url[1])){
        // Check to see if method exists in controller
        if(method_exists($this->currentController, $url[1])){
          $this->currentMethod = $url[1];
          // Unset 1 index
          unset($url[1]);
        }
      }

      // Get params
      $this->params = $url ? array_values($url) : [];

      // Call a callback with array of params
      call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
    }

    public function getUrl(){
      if(isset($_GET['url'])){
        $url = rtrim($_GET['url'], '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $url = explode('/', $url);
        return $url;
      }
    }
  }


